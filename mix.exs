defmodule Wampex.Client.MixProject do
  use Mix.Project

  def project do
    [
      app: :wampex_client,
      version: "0.1.0",
      elixir: "~> 1.9",
      start_permanent: Mix.env() == :prod,
      elixirc_paths: elixirc_paths(Mix.env()),
      aliases: aliases(),
      deps: deps(),
      docs: [
        main: "readme",
        extras: ["README.md"],
        assets: "assets"
      ]
    ]
  end

  defp elixirc_paths(:test), do: ["lib", "test/support"]
  defp elixirc_paths(_), do: ["lib"]

  def application do
    [
      extra_applications: [:logger]
    ]
  end

  defp deps do
    [
      {:credo, "~> 1.2", only: [:test], runtime: false},
      {:dialyxir, "~> 0.5.1", only: [:test], runtime: false},
      {:ex_doc, "~> 0.21", only: :dev, runtime: false},
      {:states_language, "~> 0.2"},
      {:wampex,
       git: "https://gitlab.com/entropealabs/wampex.git",
       tag: "ae6f2cfcc23dc9af94fa2c375b52bad6c1d0f75d"},
      {:websockex, "~> 0.4.2"}
    ]
  end

  defp aliases do
    [
      all_tests: [
        "compile --force --warnings-as-errors",
        "credo --strict",
        "format --check-formatted",
        "dialyzer --halt-exit-status"
      ]
    ]
  end
end
