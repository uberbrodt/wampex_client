defmodule Wampex.Client.Authentication do
  @moduledoc false

  alias Wampex.Crypto
  alias Wampex.Roles.Peer.Challenge

  @enforce_keys [:authid, :authmethods, :secret]
  defstruct [:authid, :authmethods, :secret]

  @callback handle(challenge :: Challenge.t(), auth :: __MODULE__.t()) :: {binary(), map()}

  @type t :: %__MODULE__{
          authid: binary(),
          authmethods: [binary()],
          secret: binary()
        }

  def handle(
        %Challenge{
          auth_method: "wampcra",
          extra: %{"challenge" => ch, "salt" => salt, "iterations" => it, "keylen" => len}
        },
        auth
      ) do
    {auth
     |> Map.get(:secret)
     |> Crypto.pbkdf2(salt, it, len)
     |> Crypto.hash_challenge(ch), %{}}
  end

  def handle(%Challenge{auth_method: "wampcra", extra: %{"challenge" => ch}}, auth) do
    {auth
     |> Map.get(:secret)
     |> Crypto.hash_challenge(ch), %{}}
  end
end
