defmodule Wampex.Client.Realm do
  @moduledoc "Defines the WAMP Realm"
  alias Wampex.Client.Authentication

  @enforce_keys [:name]
  defstruct [:name, :authentication]

  @type t :: %__MODULE__{
          name: binary(),
          authentication: Authentication.t() | nil
        }
end
