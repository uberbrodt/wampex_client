use Mix.Config

config :wampex_router,
  topologies: [
    kv: [
      strategy: Cluster.Strategy.Epmd,
      config: [
        hosts: []
      ],
      connect: {:net_kernel, :connect_node, []},
      disconnect: {:erlang, :disconnect_node, []},
      list_nodes: {:erlang, :nodes, [:connected]}
    ]
  ]
